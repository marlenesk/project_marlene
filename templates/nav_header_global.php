<?php
if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}
if (empty($_SESSION["userid"]) || empty($_SESSION["useremail"])) {
    include 'nav_header_public.php';
} else {
    include 'nav_header_private.php';
}
