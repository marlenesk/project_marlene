<?php
require_once "../classes/db_classes.php";
require_once "../classes/login_classes.php";
Login::checkAndRedirect('user');
?><!DOCTYPE html>
<html lang="en">
<head>

    <!--admin page header-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.1/dist/js.cookie.min.js"></script>
    <script src="../js/app.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />    
    <link rel="stylesheet" href="../style.css">
    <title>Bookshop</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg">
            <div class="container-fluid">
                <a class="navbar-brand" href="../pages/index.php">Bookshop</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <h3>Admin Page<h3>
                <div class="collapse navbar-collapse m-auto" id="navbarSupportedContent">
                    <ul class="navbar-nav justify-content-end m-auto">
                        <li class="nav-item">
                            <a class="btn btn-nav" aria-current="page" href="../php/logout_php.php">Log Out</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>              
<hr>

<!--admin manage books section-->
<div class="container-fluid  w-75">
    <div class="jumbotron">
        <div class="card mt-5">
            <div class="h2">
                <h2>Manage Books</h2>
            </div>

            <!--insert new book into db-->
            <div class="card-body">
                <form method="POST" action="../pages/adminpage.php" enctype="multipart/form-data">
                    <div class="row">
                         <div class="col-6 admin_row">
                            <input class="form-control" name="book_title" placeholder="Book Title" value="<?=$this->book_title?>">
                        </div>
                        <div class="col-6 admin_row">
                            <input class="form-control" name="book_author" placeholder="Author" value="<?=$this->book_author?>">
                         </div>
                    </div>
                    <div class="row">
                        <div class="col-8 admin_row">
                            <input class="form-control " name="book_description" placeholder="Description" value="<?=$this->book_description?>">
                        </div>
                        <div class="col-4 admin_row">
                            <input class="form-control " name="book_price" placeholder="Price" value="<?=$this->book_price?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 admin_row">
                            <input class="form-control" type="file" name="book_img" placeholder="Image" value="<?=$this->book_img?>">
                        <div>
                    </div>
                    <div>
                        <button type="submit" class="btn btn-save" value="<?=$this->save_update?>" name="button"><?=$this->save_update?></button>
                    </div>
                    <div>
                        <input type="hidden" name="book_id" value="<?=$this->book_id?>">
                    </div>
                </form>    
            </div>
        </div>    


    <!--table with existing books in db-->
    <h5 class="text-white"><?=$this->info?></h5>
        <div class="card mt-1">
            <div class="card-header">
                <form method="post" action="../pages/adminpage.php">
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search">
                         <div class="input-group-btn">
                            <button class="btn" type="submit" name="button" value="search">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>  
            <div class="card-body">
                <table class="table admin-table">
                    <thead class="bg">
                    <tr>
                        <th scope="col">Image</th>
                        <th scope="col">Title</th>
                        <th scope="col">Author</th>
                        <th scope="col">Description</th>
                        <th scope="col">Price</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <?php foreach ($this->rows as $row) : ?>
                    <tr class="t-row">
                        <td><img src="../img/<?=($row['book_img'])?>"></td>
                        <td><?=htmlspecialchars($row['book_title'])?></td>
                        <td><?=htmlspecialchars($row['book_author'])?></td>
                        <td><?=htmlspecialchars($row['book_description'])?></td>
                        <td><?=htmlspecialchars($row['book_price'])?></td>
                        <td>
                            <form method="POST">
                                <input type="hidden" name="id" value="<?=$row['book_id']?>" >
                                <button type="submit" class="btn" value="edit" name="button">edit</button>
                                <button type="submit" class="btn" value="delete" name="button">delete</button>
                            </form>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </div>    
        </div>      
    </div>
</div>    

<?php
    include "../templates/nav_footer.php";