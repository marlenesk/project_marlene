<?php
include "../templates/nav_header_global.php";
?>

    <!--checkout page-->
      <h2>Your Order</h2>

      <div class="card" style="width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">Order Details</h5>
          <a href="user.html" type="button" class="btn btn-outline-secondary">Edit</a>
          <h6 class="card-subtitle mb-2 text-muted">Adress</h6>
          <p class="card-text">Name</p>
          <p class="card-text">Streetname</p>
          <p class="card-text">Zip Code</p>
          <p class="card-text">City</p>
          <h6 class="card-subtitle mb-2 text-muted">Payment Method</h6>
          <p class="card-text">Credit Card</p>
          <p class="card-text">Card Number</p>
        </div>
      </div>
    
      <div class="list-group">
        <a href="#" class="list-group-item list-group-item-action">
          <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">Book 1</h5>
            <small>Amount</small>
            <small>Price</small>
          </div>
          <p class="mb-1">Author</p>
        </a>
          <div>
              <p class="mb-1">Shipping €</p>
              <h5 class="mb-1">Total €</h5>
              <a href="#" type="button" class="btn btn-outline-primary">Buy Now</a>
          </div>
      </div>

<?php
  include "../templates/nav_footer.php";