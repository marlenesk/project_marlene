<?php 
  $product_id = $_GET['book_id'] ?? 1;
    foreach ($books->get_rows() as $rows) :
      if($rows['book_id'] == $product_id) :
?>

    <!--display single book-->
    <div class="container">
        <div class="row">
            <div class="col">
                <img class="b-img" src="../img/<?php echo $rows['book_img']?>" alt="Book Image">
            </div>
            <div class="col">
                <h2><?php echo $rows['book_title'] ?? "Book Title"?></h2>
                <p><?php echo $rows['book_author'] ?? "Author"?></p>
                <p> 
                <?php
                    $genres = $books->get_genre_by_book($rows['book_id']);
                    foreach ($genres as $genre) {
                        echo $genre['genre_name'];
                    }
                ?>
                </p>
                <h4>€<?php echo $rows['book_price'] ?? "Price"?></h4>
                <div>
                  <input type="hidden" name="hidden_name" value="<?php echo $rows["book_title"]; ?>"/>
                  <input type="hidden" name="hidden_price" value="<?php echo $rows["book_price"]; ?>"/>
                  <input type="hidden" name="hidden_id" value="<?php echo $rows["book_id"]; ?>"/>
                  <button data-id="<?php echo $rows['book_id']?>" class="add_to_cart btn btn-sm">Add to Cart</button>
                </div>
            </div>
        </div> 
    </div>
    <div class="b-d">
        <h4>Description</h4>
        <p><?php echo $rows['book_description'] ?? "Description"?></p>
        <?php 
        endif;
        endforeach; 
        ?>
    </div> 
</body>
</html>