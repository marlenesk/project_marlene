<!--shopping cart with added products displayed-->
<div class="h2">
   <h2>Shopping Cart</h2>
</div>

<div class="table-responsive">
<table class="table table-bordered">
    <tr>
     <th>Book</th>
     <th>Quantity</th>
     <th>Price</th>
     <th>Total</th>
     <th></th>
    </tr>
   <?php
   if(isset($_COOKIE['cart']))
   {
    $total = 0;
    $cookie_data = stripslashes($_COOKIE['cart']);
    $cart_data = json_decode($cookie_data, true);
    foreach($cart_data as $rows)
    {
      $cartbook = $shoppingcart->get_book($rows['id']);
   ?>
    <tr>
     <td><?php echo $cartbook["book_title"]; ?></td>
     <td><?php echo $rows["quantity"]; ?></td>
     <td>€ <?php echo $cartbook['book_price'];?>
      </td>
     <td>€ <?php echo number_format($rows["quantity"] * $cartbook["book_price"], 2);?></td>
     <td><a class= "btn" href="../pages/cartpage.php?action=delete&id=<?php echo $rows["id"]; ?>">Remove</a></td>
    </tr>
   <?php
     $total = $total + ($rows["quantity"] * $cartbook["book_price"]);
   ?>
    <tr>
     <td colspan="3">Total</td>
     <td>€ <?php echo number_format($total, 2); ?></td>
     <td></td>
    </tr>
   <?php
    }
   }
   else
   {
    echo '
    <tr>
     <td colspan="5">No Item in Cart</td>
    </tr>
    ';
   }
   ?>
</table>
   <div>
         <a class="btn" href="#">Buy Now</a>
      </div>
   </div>
</div>
 
