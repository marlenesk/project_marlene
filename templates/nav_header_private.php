<?php
require_once "../classes/db_classes.php";
require_once "../classes/login_classes.php";
Login::checkAndRedirect('user');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.1/dist/js.cookie.min.js"></script>
    <script src="../js/app.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />    
    <link rel="stylesheet" href="../style.css">
    <title>Bookshop</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg">
            <div class="container-fluid">
                <a class="navbar-brand" href="../pages/index.php">Bookshop</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse m-auto" id="navbarSupportedContent">
                    <form method="POST" action="#" class="d-flex justify-content-center search">
                        <input class="form-control searchbar" type="search" name="search" id="search" placeholder="Search" aria-label="Search">
                        <button class="btn" type="submit"><i class="fa-solid fa-magnifying-glass"></i></button>
                    </form>
                    <div id="result"></div>
                    <ul class="navbar-nav justify-content-end m-auto">
                        <li class="nav-item">
                            <a class="btn btn-nav" aria-current="page" href="../pages/userpage.php"><i class="fa-solid fa-user"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="btn btn-nav" aria-current="page" href="../php/logout_php.php">Log Out</a>
                        </li>
                        <li class="nav-item">
                            <a class="btn btn-nav" aria-current="page" href="../pages/cartpage.php">
                                <i class="fa-solid fa-bag-shopping"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>  
        
    <div id="result"></div>   
<hr>
