<!--carousel-->
<div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="../assets/1.jpg" class="d-block w-100" alt="Slide 1">
    </div>
    <div class="carousel-item">
      <img src="../assets/2.jpg" class="d-block w-100" alt="Slide 2">
    </div>
    <div class="carousel-item">
      <img src="../assets/3.jpg" class="d-block w-100" alt="SLide 3">
    </div>
  </div>
</div>


<!--display all books in catalog as card item-->
<?php
  $allbooks = $books->get_rows();

  shuffle($allbooks);
?>
<div class="h2">
  <h2>All Books</h2>
</div>
  <div class="container">
    <div class="row">
      <div class="b_element">
      <?php foreach ($allbooks as $rows){ ?>
          <div class="card shadow col">
            <a href="<?php printf('%s?book_id=%s', 'bookpage.php',  $rows['book_id']);?>" class="card-img-top"><img width="150" height="200" src="../img/<?php echo $rows['book_img']?>" class="card-img-top" name="book_img" alt="Book Image"></a>
            <div class="card-body">
              <a href="<?php printf('%s?book_id=%s', 'bookpage.php',  $rows['book_id']);?>" class="card-title"><h5 class="card-title"><?php echo $rows['book_title'] ?? "Book Title"?></h5></a>
              <p class="card-text"><?php echo $rows['book_author'] ?? "Author"?></p>
              <p class="card-text">
              <?php
                $genres = $books->get_genre_by_book($rows['book_id']);
                  foreach ($genres as $genre) {
                    echo $genre['genre_name'];
                  }
              ?>
              </p>
              <p class="card-text">€<?php echo $rows['book_price'] ?? "Price"?></p>
            </div>
            <div>
                <input type="hidden" name="hidden_name" value="<?php echo $rows["book_title"]; ?>"/>
                <input type="hidden" name="hidden_price" value="<?php echo $rows["book_price"]; ?>"/>
                <input type="hidden" name="hidden_id" value="<?php echo $rows["book_id"]; ?>"/>
                <button data-id="<?php echo $rows['book_id']?>" class="add_to_cart btn btn-sm">Add to Cart</button>
            </div>
          </div>
      <?php } ?>
      </div>
    </div>
  </div>


