<?php
include "nav_header_global.php";
?>

    <!--user account with userdata displayed-->
      <form action="../pages/userpage.php" method="POST">
        <h2 class="h2">My Details</h2>
        <p class="user">Edit your details below if something changes so we are up to date.</p>
        <div class="mb-3 user">
          <input type="text" class="form-control" name="firstname" value="<?=$this->firstname?>">
        </div>
        <div class="mb-3 user">
          <input type="text" class="form-control" name="lastname" value="<?=$this->lastname?>">
        </div>
        <div class="mb-3 user">
            <input type="text" class="form-control" name="address" value="<?=$this->address?>">
        </div>
        <div class="mb-3 user">
            <input type="text" class="form-control" name="zip" value="<?=$this->zip?>">
        </div>
        <div class="mb-3 user">
            <input type="text" class="form-control" name="city" value="<?=$this->city?>">
        </div>
        <div class="mb-3 user">
            <input type="email" class="form-control" name="email" value="<?=$this->email?>">
        </div>
        <div>
            <button type="submit" class="btn" value="<?=$this->update?>" name="button"><?=$this->update?></button>
        </div>
        <div>
            <input type="hidden" name="id" value="<?=$this->id?>">
        </div>
      </form>

<?php
    include "../templates/nav_footer.php";