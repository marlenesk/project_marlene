-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 23. Mai 2022 um 19:17
-- Server-Version: 10.4.21-MariaDB
-- PHP-Version: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `bookshop`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `admin`
--

CREATE TABLE `admin` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `pwd` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `book`
--

CREATE TABLE `book` (
  `book_id` int(10) UNSIGNED NOT NULL,
  `book_title` varchar(255) NOT NULL,
  `book_author` varchar(255) NOT NULL,
  `book_description` varchar(255) NOT NULL,
  `book_price` int(255) NOT NULL,
  `book_img` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `book`
--

INSERT INTO `book` (`book_id`, `book_title`, `book_author`, `book_description`, `book_price`, `book_img`) VALUES
(1, 'Book 1', 'Author 1', 'Description 1', 30, '../img/mildegaben.jpeg'),
(2, 'Testbook', 'A', 'xyz', 10, '../img/dergesangderflusskrebse.jpeg'),
(3, 'Book 2', 'Author 2', 'Description 2', 20, '../img/einefragederchemie.jpeg'),
(4, 'Book 3', 'Author 3', 'Description 3', 20, '../img/sagendesklassischenaltertums.jpeg');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `books_in_genre`
--

CREATE TABLE `books_in_genre` (
  `big_book_id` int(11) UNSIGNED NOT NULL,
  `big_genre_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `books_in_order`
--

CREATE TABLE `books_in_order` (
  `bio_book_id` int(10) UNSIGNED NOT NULL,
  `bio_order_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `genre`
--

CREATE TABLE `genre` (
  `genre_id` int(10) UNSIGNED NOT NULL,
  `genre_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `genre`
--

INSERT INTO `genre` (`genre_id`, `genre_name`) VALUES
(1, 'Krimi'),
(2, 'Thriller');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `order`
--

CREATE TABLE `order` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `pricetotal` int(11) NOT NULL,
  `paymentmethod` int(11) NOT NULL,
  `user_order_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pwd` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `payment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `email`, `pwd`, `address`, `zip`, `city`, `payment`) VALUES
(1, 'Marlene', 'K', 'm.k@mail.com', '$2y$10$FsybAW8g9aTzG7jJXMY.hOvkA8hsyAEEbClzPtv8RK4Tiv4Ffj.hy', 'Gasse', '1010', 'Wien', ''),
(2, 'Test', 'T', 'test.t@mail.com', '$2y$10$KOsAoGRDuL98uUIf0AAm8.BfUm3B9lAghbiMgZwQkLPcwA6PoVIxa', 'xy', '1111', 'Wien', ''),
(3, 'test2', 'test', 'test2.t@mail.com', '$2y$10$5mSp/lcKGTtngLu1YJhqH.azh.Qj8rHJfa8sL0UHbsOsPKtRcCLX6', 'test', '5', 'Wien', ''),
(4, 'Test3', 'test', 'test.3@mail.com', '$2y$10$BvaCk2khq3j9of33tfDXu.P49Hmp2UORKMRe0Cz1mprRalz4D0hzW', 'asdfg', '1', 'wien', '');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`book_id`);

--
-- Indizes für die Tabelle `books_in_genre`
--
ALTER TABLE `books_in_genre`
  ADD PRIMARY KEY (`big_book_id`,`big_genre_id`),
  ADD KEY `big_genre_id` (`big_genre_id`);

--
-- Indizes für die Tabelle `books_in_order`
--
ALTER TABLE `books_in_order`
  ADD PRIMARY KEY (`bio_book_id`,`bio_order_id`),
  ADD KEY `bio_order_id` (`bio_order_id`);

--
-- Indizes für die Tabelle `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`genre_id`);

--
-- Indizes für die Tabelle `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_order_id` (`user_order_id`);

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `book`
--
ALTER TABLE `book`
  MODIFY `book_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT für Tabelle `genre`
--
ALTER TABLE `genre`
  MODIFY `genre_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT für Tabelle `order`
--
ALTER TABLE `order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `books_in_genre`
--
ALTER TABLE `books_in_genre`
  ADD CONSTRAINT `books_in_genre_ibfk_1` FOREIGN KEY (`big_book_id`) REFERENCES `book` (`book_id`),
  ADD CONSTRAINT `books_in_genre_ibfk_2` FOREIGN KEY (`big_genre_id`) REFERENCES `genre` (`genre_id`);

--
-- Constraints der Tabelle `books_in_order`
--
ALTER TABLE `books_in_order`
  ADD CONSTRAINT `books_in_order_ibfk_1` FOREIGN KEY (`bio_book_id`) REFERENCES `book` (`book_id`),
  ADD CONSTRAINT `books_in_order_ibfk_2` FOREIGN KEY (`bio_order_id`) REFERENCES `order` (`id`);

--
-- Constraints der Tabelle `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_ibfk_1` FOREIGN KEY (`user_order_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
