<?php
include "../templates/nav_header_global.php";

require_once '../classes/db_classes.php';
require_once '../classes/books_classes.php';
require_once '../classes/cart_classes.php';

$shoppingcart = new Cart();

include "../templates/cart.php";

include "../templates/nav_footer.php";