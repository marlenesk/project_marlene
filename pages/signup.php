<?php
include "../templates/nav_header_global.php";
?><body>
    <?php
    if (isset($_GET["error"])) {
      echo $_GET["error"];
    }
    ?>

     <!--signup form-->
    <form class="row g-3" action="../php/signup_php.php" method="post">
        <div class="col-md-4 login-form">
          <label for="validationDefault01" class="form-label">First name</label>
          <input type="text" class="form-control" name="firstname" required>
        </div>
        <div class="col-md-4 login-form">
          <label for="validationDefault02" class="form-label">Last name</label>
          <input type="text" class="form-control" name="lastname" required>
        </div>
        <div class="col-md-4 login-form">
          <label for="validationDefault02" class="form-label">E-Mail</label>
          <input type="email" class="form-control" name="email" required>
        </div>
        <div class="col-md-4 login-form">
          <label for="validationDefault02" class="form-label">Password</label>
          <input type="password" class="form-control" name="pwd" required>
        </div>
        <div class="col-md-4 login-form">
          <label for="validationDefault02" class="form-label">Repeat Password</label>
          <input type="password" class="form-control" name="pwdr" required>
        </div>
        <div class="col-md-4 login-form">
          <label for="validationDefault03" class="form-label">Address</label>
          <input type="text" class="form-control" name="address" required>
        </div>
        <div class="col-md-4 login-form">
          <label for="validationDefault05" class="form-label">Zip Code</label>
          <input type="text" class="form-control" name="zip" required>
        </div>
        <div class="col-md-4 login-form">
          <label for="validationDefault03" class="form-label">City</label>
          <input type="text" class="form-control" name="city" required>
        </div>
        </div>
        <div class="col-12">
          <button class="btn" type="submit" name="submit">Sign Up</button>
        </div>
      </form>
<?php
  include "../templates/nav_footer.php";