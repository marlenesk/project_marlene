<?php
include "../templates/nav_header_global.php";
?><body>
    <?php
    if (isset($_GET["error"])) {
      echo $_GET["error"];
    }
    ?>

        <!--login form-->
    <form class="row g-3" action="../php/login_php.php" method="post">
        <div class="col-md-4 login-form">
          <label for="validationDefault02" class="form-label">E-Mail</label>
          <input type="email" class="form-control" name="email" required>
        </div>
        <div class="col-md-4">
          <label for="validationDefault02" class="form-label">Password</label>
          <input type="password" class="form-control" name="pwd" required>
        </div>
        <div class="col-12">
          <button class="btn" type="submit" name="submit">Log In</button>
        </div>
      </form>
<?php
      include "../templates/nav_footer.php";