<?php

/**
 * this class handles the signup data from users. 
 */
class SignUp extends Db {


/**
 * this method inserts the new userdata into the database.
 */
    protected function setuser($firstname, $lastname, $email, $pwd, $address, $zip, $city){
        $stmt = $this->connect()->prepare('INSERT INTO user (firstname, lastname, email, pwd, address, zip, city) VALUES (?,?,?,?,?,?,?)');

        $hashedpwd = password_hash($pwd, PASSWORD_DEFAULT);


        if(!$stmt->execute(array($firstname, $lastname, $email, $hashedpwd, $address, $zip, $city))){
            $stmt = null;
            header("location: ../pages/signup.php?error=stmtfailed");
            exit();
        }

        $stmt = null;
    }


/**
 * this method checks if the email is already in the db.
 */
    protected function checkemail($email){
        $stmt = $this->connect()->prepare('SELECT id FROM user WHERE email = ?');

        if(!$stmt->execute([$email])){
            $stmt = null;
            header("location: ../pages/signup.php?error=stmtfailed");
            exit();
        }

        if($stmt->rowCount() > 0){
            $resultcheck = false;
        }
        else {
            $resultcheck = true;
        }
        return $resultcheck;
    }


}