<?php

/**
 * this class executes the signup.
 */
class SignupContr extends SignUp{

    private $firstname;
    private $lastname;
    private $email;
    private $pwd;
    private $pwdr;
    private $address;
    private $zip;
    private $city;

    public function __construct($firstname, $lastname, $email, $pwd, $pwdr, $address, $zip, $city){

        $this->firstname=$firstname;
        $this->lastname=$lastname;
        $this->email=$email;
        $this->pwd=$pwd;
        $this->pwdr=$pwdr;
        $this->address=$address;
        $this->zip=$zip;
        $this->city=$city;

    }

/**
 * this method checks the entered data and signs the user up.
 */
    public function signup(){

        if($this->noinput() == false){
            header("location: ../pages/signup.php?error=noinput");
            exit();
        }
        if($this->invalidname() == false){
            header("location: ../pages/signup.php?error=invalidname");
            exit();
        }
        if($this->invalidemail() == false){
            header("location: ../pages/signup.php?error=invalidemail");
            exit();
        }
        if($this->pwdmatch() == false){
            header("location: ../pages/signup.php?error=unmatchingpasswords");
            exit();
        }
        if($this->accountcheck() == false){
            header("location: ../pages/signup.php?error=emailtaken");
            exit();
        }
        
        $this->setuser($this->firstname, $this->lastname, $this->email, $this->pwd, $this->address, $this->zip, $this->city);
    }

    //error handling

/**
 * this method checks if any field is left empty.
 */
    private function noinput(){

        if(empty($this->firstname) || empty($this->lastname) || empty($this->email) || empty($this->pwd) || empty($this->pwdr) || empty($this->address) || empty($this->zip) || empty($this->city)){
            $result = false;
        }
        else{
            $result = true;
        }
        return $result;
    }


/**
 * this method checks if the entered name is valid. 
 */
    private function invalidname(){

        if(!preg_match("/^[a-zA-Z0-9]*$/", $this->firstname) || !preg_match("/^[a-zA-Z0-9]*$/", $this->lastname)) {
            $result = false;
        }
        else{
            $result = true;
        }
        return $result;
    }


/**
 * this method checks if the entered email is valid.
 */
    private function invalidemail(){

        if(!filter_var($this->email, FILTER_VALIDATE_EMAIL)){
            $result = false;
        }
        else{
            $result = true;
        }
        return $result;
    }


/**
 * this method checks if the passwords match each other. 
 */
    private function pwdmatch(){

        if($this->pwd !== $this->pwdr){
            $result = false;
        }
        else{
            $result = true;
        }
        return $result;
    }


/**
 * this method checks if the account already exists.
 */
    private function accountcheck(){

        if(!$this->checkemail($this->email)){
            $result = false;
        }
        else{
            $result = true;
        }
        return $result;
    }
}