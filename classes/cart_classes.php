<?php

/**
 * this class handles books that are added to cart. 
 */
class Cart extends Books {


  /*  public function add_to_cart(){
        if(isset($_POST["add_to_cart"])){
            if(isset($_COOKIE["shopping_cart"])){
                $cookie_data = stripslashes($_COOKIE['shopping_cart']);
                $cart_data = json_decode($cookie_data, true);
            }
            else{
            $cart_data = array();
            }

            $book_id_list = array_column($cart_data, 'book_id');

            if(in_array($_POST["hidden_id"], $book_id_list)){
                foreach($cart_data as $rows){
                    if($cart_data[$rows]["book_id"] == $_POST["hidden_id"]){
                        $cart_data[$rows]["item_quantity"] = $cart_data[$rows]["item_quantity"] + $_POST["quantity"];
                    }
                }
            }
            else{
                $item_array = array(
                'book_id'   => $_POST["hidden_id"],
                'book_title'   => $_POST["hidden_name"],
                'book_price'  => $_POST["hidden_price"],
                'item_quantity'  => $_POST["quantity"]);
                $cart_data[] = $item_array;
            }

        
            $item_data = json_encode($cart_data);
                setcookie('shopping_cart', $item_data, time() + (86400 * 30));
                header("location:#?success=1");
        }
    }
*/

/**
 * this method deletes a certain book from the cart. 
 */
    public function delete_cart_item (){
        if(isset($_GET["delete"])){
                $cookie_data = stripslashes($_COOKIE['cart']);
                $cart_data = json_decode($cookie_data, true);
            foreach($cart_data as $rows){
                if($cart_data[$rows]['id'] == $_GET["id"]){
                    unset($cart_data[$rows]);
                    $item_data = json_encode($cart_data);
                    setcookie("cart", $item_data, time() + (86400 * 30));
                    header("location:#?remove=1");
                }
            }
        }
    }


/**
 * this method clears the whole cart with all books inside. 
 */
    public function clear_cart () {
        if(isset($_GET["clear"])){
            setcookie("cart", "", time() - 3600);
            header("location:#?clearall=1");
        }
    }
    }

