<?php

/**
 * this class handles user account data from the database.
 */
class User extends Db {

    
    protected $id;
    protected $firstname;
    protected $lastname;
    protected $email;
    protected $address;
    protected $zip;
    protected $city;
    protected $payment;

    protected $row;
    protected $list_sql="select * from user";
    protected $param=[];
    protected $update="Update";
    protected $action;


/**
 * this method fetches the user data of the logged in user.
 */
    protected function get_row() {
        $stmt= $this->connect()->prepare('SELECT * FROM user WHERE id=:id');
        $stmt->execute(['id' => $_SESSION['userid']]);
        $row=$stmt->fetch(PDO::FETCH_ASSOC);
        $this->id = $row['id'];
        $this->firstname = $row['firstname'] ?? 'First Name';
        $this->lastname = $row['lastname'] ?? 'Last Name';
        $this->email = $row['email'] ?? 'Email';
        $this->address = $row['address'] ?? 'Address';
        $this->zip=$row['zip'] ?? 'Zip Code';
        $this->city=$row['city'] ?? 'City';
        $this->payment=$row['payment'] ?? 'Payment';
    }


/**
 * this method displays the user data and prepares for the update method.
 */
    protected function edit(){
        $sql = "SELECT * FROM user WHERE id=:id";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute(['id'=>$_POST['id']]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $this->firstname = $row['firstname'] ?? 'First Name';
        $this->lastname = $row['lastname'] ?? 'Last Name';
        $this->email = $row['email'] ?? 'Email';
        $this->address = $row['address'] ?? 'Address';
        $this->zip=$row['zip'] ?? 'Zip Code';
        $this->city=$row['city'] ?? 'City';
        $this->payment=$row['payment'] ?? 'Payment';
    }

/**
 * this method updates the edited user data and inserts it into the db.
 */
    protected function update(){
        $sql = "UPDATE user SET 
                    firstname=:firstname,
                    lastname=:lastname,
                    email=:email,
                    address=:address,
                    zip=:zip,
                    city=:city,
                    payment=:payment
                WHERE id=:id";
        $stmt = $this->connect()->prepare($sql);
        
        $stmt->execute([
            'firstname' => $_POST['firstname'] ?? 'First Name',
            'lastname' => $_POST['lastname'] ?? 'Last Name',
            'email' => $_POST['email'] ?? 'Email',
            'address' => $_POST['address'] ?? 'Address',
            'zip' => $_POST['zip'] ?? 'Zip',
            'city' => $_POST['city'] ?? 'City',
            'payment' => $_POST['payment'] ?? 'Payment',
            'id' => $_POST['id'] ?? 'Id'
        ]);
        if ($stmt->rowCount() > 0) $this->info = $stmt->rowCount() . " Information updated!";
        else $this->info = "No Information updated!";
    }
}


/**
 * this class includes the user.php html template.
 */
class UserTemplate extends User{
    function render(){
        include "../templates/user.php";
    }
}


/**
 * this class executes the methods of user class.
 */
class UserContr extends UserTemplate {

/**
 * this method edits/updates the user data on button click.
 */
    function __construct(){
        if(isset($_POST['button'])) {
            $action = $_POST['button'];
            switch ($action) {
                case 'Update':
                    $this->update();
                    break;
                case 'edit':
                    $this->edit();
                    break;
            }
        }

        $this->get_row();
        $this->render();
    }
}
