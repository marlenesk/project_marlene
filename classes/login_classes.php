<?php

/**
 * this class manages the login data of registered users and admins.
 */
abstract class Login extends Db {


/**
 * this method fetches the user/admin from the database and checks their email and password.
 */
    protected function getuser() {
        $stmt = $this->connect()->prepare('SELECT pwd, id, email FROM '.$this->type.' WHERE email = ?');

        if ($this->type == 'admin') {
            $redirect = 'admin_login.php';
        } else {
            $redirect = 'login.php';
        }

        if(!$stmt->execute(array($this->email))){
            $stmt = null;
            header("location: ../pages/$redirect?error=stmtfailed");
            exit();
        }

        $pwdhashed = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($pwdhashed) === 0) {
            $stmt = null;
            header("location: ../pages/$redirect?error=usernotfound");
            exit();
        }
        $checkpwd = password_verify($this->pwd, $pwdhashed[0]["pwd"]);

        if($checkpwd == false){

            $stmt = null;
            header("location: ../pages/$redirect?error=wrongpassword");
            exit();
        }
        else {

            $_SESSION["userid"] = $pwdhashed[0]["id"];
            $_SESSION["useremail"] = $pwdhashed[0]["email"];
            $_SESSION["type"] = $this->type;

            $stmt = null;
        }
    }


/**
 * this method redirects the admin to a separate admin login.
 */
    public static function checkAndRedirect($type = false) {
        if (empty($_SESSION["userid"]) || empty($_SESSION["useremail"])) {
            if (!$type) {
                header("Location: ../pages/signup.php");
            } elseif ($type == 'admin') {
                header("Location: ../pages/admin_login_php.php");
            }
        }
    }
}