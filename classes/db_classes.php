<?php

/**
 * this class handles the connection to the database. 
 */
class Db {

/** 
 *this method connects to the database.
 */
    protected function connect(){
        try {
            $username="root";
            $password="";
            $conn= new PDO('mysql:host=localhost;dbname=bookshop', $username, $password);
            return $conn;
        }
        catch(PDOException $e) {
            echo "This page is currently not available. Please try again later.";
            die();
        }
    }
}