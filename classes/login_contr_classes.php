<?php

/**
 * this class logs the user/admin in.
 */
class LoginContr extends Login {

    protected $email;
    protected $pwd;
    protected $type;

    public function __construct($email, $pwd, $type = 'user'){

        $this->email=$email;
        $this->pwd=$pwd;
        $this->type=$type;
    }


/**
 * this method executes the login.
 */
    public function login(){

        if($this->noinput() == false) {
            if ($this->type == 'admin') {
                header("location: ../pages/admin_login.php?error=noinput");
            } else {
                header("location: ../pages/login.php?error=noinput");
            }
            exit();
        }
        
        $this->getuser();
    }

    //error handling

    private function noinput(){

        if(empty($this->email) || empty($this->pwd)){
            $result = false;
        }
        else{
            $result = true;
        }
        return $result;
    }
}