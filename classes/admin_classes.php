<?php
/**
 * this class lets admin users administer books from the database. 
 */
class Admin extends Db {

    
    protected $book_id;
    protected $book_title;
    protected $book_author;
    protected $book_description;
    protected $book_price;
    protected $book_img;
    protected $genre_name;

    protected $row;
    protected $list_sql="select * from book";
    protected $param=[];
    protected $info = "";
    protected $save_update="save";
    protected $action="none";


/**
 * this method fetches all books from the database. 
 */
    protected function get_rows(){
        $stmt= $this->connect()->prepare($this->list_sql);
        $stmt->execute($this->param);
        $this->rows=$stmt->fetchAll(PDO::FETCH_ASSOC);
    }


/**
 * this method returns the book genre.
 */
    public function get_genre_by_book($id) {
        $stmt= $this->connect()->prepare('SELECT * FROM books_in_genre JOIN genre ON big_genre_id = genre_id WHERE big_book_id = ?');
        $stmt->execute([$id]);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }



/**
 * this method inserts a new book into the database. 
 */
    protected function save(){
        if($_POST['book_title']=="" 
            OR $_POST['book_author']=="" 
            OR $_POST['book_description']=="" 
            OR $_POST['book_price']=="" 
            OR $_FILES['book_img']['name']=="") {
            $info = "Please fill in all fields!";
            echo $info;
            return;
        }    
        $sql = "INSERT INTO book (book_title, book_author, book_description, book_price, book_img) 
                VALUES (:book_title, :book_author, :book_description, :book_price, :book_img)";
        $stmt = $this->connect()->prepare($sql);

        $target_dir = "../img/";
        $target_file = $target_dir . basename($_FILES['book_img']['name']);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        if(isset($_POST["button"])) {
        $check = getimagesize($_FILES["book_img"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }
        }

        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
          }
          
         if ($_FILES["book_img"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
          }
          
          if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
            echo "Sorry, only JPG, JPEG & PNG files are allowed.";
            $uploadOk = 0;
          }

          if ($uploadOk === 1) {
            if (move_uploaded_file($_FILES["book_img"]["tmp_name"], $target_file));
            $stmt->execute([
                'book_title'       => $_POST['book_title'],
                'book_author'      => $_POST['book_author'],
                'book_description' => $_POST['book_description'],
                'book_price'       => $_POST['book_price'],
                'book_img'         => $_FILES['book_img']['name']
            ]);
            if($stmt->rowCount()>0) $info=$stmt->rowCount()." Book added to catalog!";
            else $info="No Book added!";
          }
    }


/** 
 * this method fetches book data from database and displays it, allowing the admin to edit it.
 */
    protected function edit(){
        $this->save_update="update";
        $sql = "SELECT * FROM book WHERE book_id=:book_id";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute(['book_id'=>$_POST['id']]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $this->book_title = $row['book_title'];
        $this->book_author = $row['book_author'];
        $this->book_description = $row['book_description'];
        $this->book_price = $row['book_price'];
        $this->book_img = $row['book_img'];
        $this->book_id=$row['book_id'];
    }



/**
 * this method updates an edited book to database.
 */
    protected function update(){
        $sql = "UPDATE book SET 
                    book_title=:book_title,
                    book_author=:book_author,
                    book_description=:book_description,
                    book_price=:book_price,
                    book_img=:book_img
                WHERE book_id=:book_id";
        $stmt = $this->connect()->prepare($sql);
        
        $stmt->execute([
            'book_title' => $_POST['book_title'],
            'book_author' => $_POST['book_author'],
            'book_description' => $_POST['book_description'],
            'book_price' => $_POST['book_price'],
            'book_img' => $_FILES['book_img']['name'],
            'book_id' => $_POST['book_id']
        ]);
        if ($stmt->rowCount() > 0) $this->info = $stmt->rowCount() . " Book updated!";
        else $this->info = "No Book updated!";
    }



/**
 * this method deletes a book from database. 
 */
    protected function delete(){
        $sql = "DELETE FROM book WHERE book_id=:book_id";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute(['book_id'=>$_POST['id']]);
        if ($stmt->rowCount() > 0) $this->info = $stmt->rowCount() . " Book deleted!";
        else $this->info = "No Book deleted!";
    }


/**
 * this method searches books from database. 
 */
    public function search(){
        $this->list_sql = "select * from book where (book_title like :search_string) or (book_author like :search_string)";
        $this->param = ['search_string' => '%' . $_POST['search'] . '%'];
    }

}


/**
 * this class includes the admin.php html template.
 */
class AdminTemplate extends Admin{
    function render(){
        include "../templates/admin.php";
    }
}


/**
 * this class executes the methods of the former admin class when the admin clicks a button in the admin.php template.
 */
class AdminContr extends AdminTemplate {

/**
 * this method executes the methods of the admin class on button click. 
 */
    function __construct(){
        if(isset($_POST['button'])) {
            $action = $_POST['button'];
            switch ($action) {
                case 'save':
                    $this->save();
                    break;
                case 'edit':
                    $this->edit();
                    break;
                case 'update':
                    $this->update();
                    break;
                case 'delete':
                    $this->delete();
                    break;
                case 'search':
                    $this->search();
            }
        }
        $this->get_rows();
        $this->render();
    }
}
