<?php

/**
 * this class fetches books from the database.
 */
class Books extends Db {


/**
 * this method fetches all books from database.
 */
    public function get_rows(){
        $stmt = $this->connect()->query("SELECT * FROM book");
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


/**
 * this method fetches a certain book from database.
 */
    public function get_book($book_id = null){
        if(isset($book_id)){
            $stmt = $this->connect()->prepare("SELECT * FROM book WHERE book_id=?");
            $stmt->execute([$book_id]);
            return $stmt->fetch(PDO::FETCH_ASSOC);
        }
    }


/**
 * this method returns the genre by book. 
 */
    public function get_genre_by_book($id) {
        $stmt= $this->connect()->prepare('SELECT * FROM books_in_genre JOIN genre ON big_genre_id = genre_id WHERE big_book_id = ?');
        $stmt->execute([$id]);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


/**
 * this method searches for a specific book from the book table.
 */
    public function search_book($book_id = null){
        if(isset($book_id)){
            $stmt = $this->connect()->prepare("SELECT * FROM book WHERE book_title LIKE '%".$POST['book_title']."%'");
            $stmt->execute([$book_id]);
            return $stmt->fetch(PDO::FETCH_ASSOC);
        }
    }
}
