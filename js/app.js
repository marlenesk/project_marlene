/**
 * add item to cart
 */
$(function() {
    $('.add_to_cart').on('click', function() {
        const val = $(this).data('id');
        let current = Cookies.get('cart');
        if (current === undefined) {
            current = [{
                id: val,
                quantity: 1
            }];
        } else {
            current = JSON.parse(current);
            console.log(current);
            let newBook = true;
            for (el of current) {
                if (el.id === val) {
                    el.quantity++;
                    newBook = false;
                }
            }
            if (newBook) {
                current.push({
                    id: val,
                    quantity: 1
                });
            }
        }
        current = JSON.stringify(current);
        Cookies.set('cart', current, { expires: 7 });
    });
});


/**
 * delete cart item from cart
 */
$(function() {
    $('.clear').on('click', function() {
        const val = $(this).data('id');
        let current = Cookies.set('cart', current, {expires: -1});
    });
});


/**
 * searchbar
 */
$(document).ready(function(){
    $('#search').keyup(function(){
        var book_title = $(this).val();
        $.post('../php/search.php', {book_title:book_title}, function(data){
            $('div#result').css({'display':'block'});
            $('div#result').html(data);

        });
    });
});